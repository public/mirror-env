# Mirror Playbook
**DO NOT run this on your host machine!**

Run this playbook as root **inside** the vm to set up the mirror's services
```
ansible-playbook main.yml
```