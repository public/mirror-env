# Mirror Env
This repo helps quickly set up a vm development environment for cs club's
mirrors using ansible.

There are two options for getting the mirror dev vm running:
- qemu (automated install)
- libvirt (manual install)

Follow the relevant instructions below. Please leave an issue for any bugs you
find.

## VM Installation Option 1 (qmeu)
Follow instructions in the [readme](qemu/README.md) of the qemu directory.

## VM Install Option 2 (libvirt)
Follow the instructions in [readme](libvirt/README.md) of the libirt directory.

## Issues/Remarks
- vm is set to autostart when installing using libvirt playbook
- cron config `/etc/cron.d/csc-mirror` is commented out for now
- merlin.service is NOT started
- outsider/insider
  - ssh server for push user is running on port 23
  - `/etc/nginx/includes/mirror.conf` allow
  - `/etc/rsyncd.conf` hosts allow
- commented out
  - nginx server_name `debian.csclub.uwaterloo.ca`, `ca.ceph.com`, `ca.releases.ubuntu.com`
  - nginx port 443 configuration

```
$ lsblk
NAME           MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINT
vda            252:0    0   10G  0 disk  
├─vda1         252:1    0  500M  0 part  /boot/efi
└─vda2         252:2    0    9G  0 part  
  └─md0          9:0    0    9G  0 raid1 
    └─vg0-root 253:0    0    8G  0 lvm   /
vdb            252:16   0   10G  0 disk  
├─vdb1         252:17   0  500M  0 part  
└─vdb2         252:18   0    9G  0 part  
  └─md0          9:0    0    9G  0 raid1 
    └─vg0-root 253:0    0    8G  0 lvm   /
vdc            252:32   0   10G  0 disk  
├─vdc1         252:33   0   10G  0 part  
└─vdc9         252:41   0    8M  0 part  
vdd            252:48   0   10G  0 disk  
├─vdd1         252:49   0   10G  0 part  
└─vdd9         252:57   0    8M  0 part  
vde            252:64   0   10G  0 disk  
├─vde1         252:65   0   10G  0 part  
└─vde9         252:73   0    8M  0 part  
vdf            252:80   0   10G  0 disk  
├─vdf1         252:81   0   10G  0 part  
└─vdf9         252:89   0    8M  0 part  
```
