# Installation

Install the following:
- ansible
- qemu
- genisoimage
- ovmf (find the location of `OVMF\_CODE.fd`, it is system dependent)

Before doing anything else, edit the vars in `hosts` to your system's needs. 
For qemu installation specifically, you need to provide the location of your 
`OVMF_CODE.fd` file.

To begin the setup process, in this folder, run:
```
$ ansible-playbook -K install.yml
```

To view the vm, you also need a vnc viewer.
[TigerVNC](https://github.com/TigerVNC/tigervnc) is a good choice.

Simply run
```
$ vncviewer :5900
```

Currently, this is a bug with the installation process that causes the vm to
not terminate after completing the base install, causing the ansible task to
hang. You can manually terminate ansible once the vm shows:

![installation complete](images/installation_complete.png)

Once the base installation is complete, we need to configure the system in a
postinstall step, start by running the vm with:
```
$ ansible-playbook run.yml
```

And then run the postinstall playbook:
```
$ ansible-playbook postinstall.yml
```

The mirror dev environment is now ready to use. All future vm startups can be
done with:
```
$ ansible-playbook run.yml
```

The default login user has
```
username: local
password: password
```

