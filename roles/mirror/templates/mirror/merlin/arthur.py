#!/usr/bin/python2

import socket, sys
try:
    command = sys.argv[1]
except:
    print("bad command")
    sys.exit(1)


s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
s.connect('/home/mirror/merlin/merlin.sock')
s.send(command)
s.shutdown(socket.SHUT_WR)
response = ''
while True:
    data = s.recv(4096)
    if not data:
        break
    response = response + data
s.close()
if command=="status":
    print(response)
else:
    print(response)
