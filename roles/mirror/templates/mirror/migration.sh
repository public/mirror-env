#!/bin/bash

# Non-directories in root
export FILES='index.html index.css stats.png'

# Include trailing slash on all paths
HOME=/srv/mirror/
export SOURCE='/mirror/root/'
export DESTINATION="/mirror/newroot/.cscmirror1/"
export LOGS="/var/log/mirror-newroot/"
export LOCKS="/tmp/mirror-locks/"

export RSYNC_FLAGS='-aHAv --quiet --stats --delay-updates --delete-after --partial --delete --numeric-ids --timeout=3600 --exclude .~tmp~/'

sync_project() {
   if [ $# -ne 1 ]; then
      echo 'Usage:' $0 'project|file' >&2
      return 1
   fi

   # only 1 active sync for this project
   exec 9>"${LOCKS}$1.lck"
   flock -n 9 || return 0

   # delay a random time (<= 5 mins)
   # that way all projects don't sync at once
   #sleep $((RANDOM%300))

   if [[ $FILES == *"$1"* ]]; then
      nice rsync $RSYNC_FLAGS --log-file="${LOGS}$1.log" "${SOURCE}$1" "${DESTINATION}" >/dev/null 2>&1 
   else
      nice rsync $RSYNC_FLAGS --log-file="${LOGS}$1.log" "${SOURCE}$1/" "${DESTINATION}$1/" >/dev/null 2>&1 
   fi
}
export -f sync_project

if [ ! -d "${LOGS}" ]; then
	mkdir -p "${LOGS}"
fi

if [ ! -d "${LOCKS}" ]; then
	mkdir -p "${LOCKS}"
fi

rsync --list-only "${SOURCE}" | grep -v '^[\*\.]' | awk '{print $5}' | grep '^[a-zA-Z0-9]' | parallel --no-notice --jobs 8 sync_project
