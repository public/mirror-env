#!/bin/sh

cd ~mirror
PROG=`/opt/MegaCli/MegaCli64 -ldrecon -showprog -l1 -a0 | grep Completed |sed 's/.*Completed \([0-9]*\)%.*/\1/'`
sed -i "s/<div id=\"raid-progress\">.*<\/div>/<div id=\"raid-progress\">Reconstruction of the RAID has completed $PROG%<\/div>/" index.html
